// extern crate r2d2;
// extern crate r2d2_mysql;
//
// use r2d2_mysql::mysql::{OptsBuilder, QueryResult, from_row};
// use std::sync::Arc;
// use r2d2::Pool;
// use r2d2_mysql::MysqlConnectionManager;

use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use listenfd::ListenFd;
use actix_web::get;
mod controllers;

// This struct represents state
struct AppState {
    app_name: String,
    // pool: Arc<Pool<MysqlConnectionManager>>,
}

// fn get_pool() -> Option<Arc<Pool<MysqlConnectionManager>>> {
//     let mut o = OptsBuilder::new();
//     o.db_name(Option::Some("perspectiva"));
//     o.user(Option::Some("root"));
//     o.pass(Option::Some(""));
//
//     let manager = r2d2_mysql::MysqlConnectionManager::new(o);
//
//     println!("Getting pool");
//
//     let pool = Arc::new(r2d2::Pool::new(manager).unwrap());
//     return Option::Some(pool);
// }


async fn index() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

// #[get("/mysql")]
// async fn mysql() -> impl Responder {
//     let app_name = &data.app_name; // <- get app_name
//
//     let pool = &data.pool;
//
//
//     let pool = pool.clone();
//     let mut conn = pool.get().unwrap();
//
//     let param:u16 = 90114445;
//     let qr: QueryResult = conn.prep_exec("select * from `a_deal` where `reg` = ?", (param, )).unwrap();
//
//     let mut rec: Option<(i32, String)> = None;
//
//     for row in qr {
//         rec = Some(from_row(row.unwrap()));
//         break;
//     }
//
//     let unwrap_rec = rec.unwrap();
//     format!("Hello {} ({})! \n from {}",  unwrap_rec.1, unwrap_rec.0, app_name)
// }

#[actix_rt::main]
async fn main() -> std::io::Result<()> {

    let mut listenfd:ListenFd = ListenFd::from_env();
    let mut server = HttpServer::new(||
        App::new()
            .data(AppState {
                app_name: String::from("ActApi"),
            })
            .route("/", web::get().to(index))
            // .route("/mysql", web::get().to(mysql))


            .service(web::scope("/api").configure(controllers::v1::dictionary_controller::init ))
    );

    server = if let Some(l) = listenfd.take_tcp_listener(0).unwrap() {
        server.listen(l)?
    } else {
        server.bind("127.0.0.1:3000")?
    };

    server.run().await
}
