
    use actix_web::{web, HttpResponse, Responder};
    use actix_web::get;



    pub fn init(cfg: &mut web::ServiceConfig) {
        cfg.service(
            web::resource("/dictionary")
                .route(web::get().to(|| HttpResponse::Ok().body("test")))
        ).service(base_info);
    }

    #[get("/dictionary/test")]
    async fn base_info() -> impl Responder {
        let testik:u8 = 12;

        HttpResponse::Ok().json(testik)
        // HttpResponse::Ok().body("<h1 style='color: gold;'>Hey there!</h1>")
    }


